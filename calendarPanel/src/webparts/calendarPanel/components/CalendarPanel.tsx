import * as React from 'react';
import styles from './CalendarPanel.module.scss';
import { ICalendarPanelProps } from './ICalendarPanelProps';
import { escape } from '@microsoft/sp-lodash-subset';
import { HttpClient, IHttpClientOptions, HttpClientResponse, SPHttpClient, ISPHttpClientOptions, SPHttpClientResponse } from '@microsoft/sp-http';

export interface ICalendarPanelState {
  data:any;
  eventListName:any;
  category:any;
}
export default class CalendarPanel extends React.Component<ICalendarPanelProps, ICalendarPanelState>{
 constructor(props){
   super(props);
   this.state={
     data:[],
     eventListName:[],
     category:[],
   }
   this.getEventsList=this.getEventsList.bind(this);
   this.getCatgeory=this.getCatgeory.bind(this);

 }
private data:any=[];
private new:any=[];
  private getEventsList(){
    var lists=[];
    let qurl="/_api/web/lists/";
    const opt: ISPHttpClientOptions = { headers: { 'Content-Type': 'application/json;odata=verbose' } };
    this.props.SPHttpClient.get(this.props.pagecontext.web.absoluteUrl + qurl, SPHttpClient.configurations.v1, opt)
      .then((response: SPHttpClientResponse) => {
        response.json().then((json: any) => {
          let data=json;
         
     
          for (let i=0;i<json.value.length;i++) {// event list schema
            if(json.value[i].TemplateFeatureId =="00bfea71-ec85-4903-972d-ebe475780106"){
            lists=[json.value[i].Title]; 
            this.setState({eventListName:[...this.state.eventListName,lists]});
         
            }  
        }   
    
      

        })
      })
      return lists;
    }


  private getCatgeory(eventList="Events"){
   
    let qurl=`/_api/web/lists/getbytitle('${eventList}')/items`;
    const opt: ISPHttpClientOptions = { headers: { 'Content-Type': 'application/json;odata=verbose' } };
    this.props.SPHttpClient.get(this.props.pagecontext.web.absoluteUrl + qurl, SPHttpClient.configurations.v1, opt)
      .then((response: SPHttpClientResponse) => {
        response.json().then((json: any) => {
            for(let i=0; i<json.value.length;i++){
              let last=json.value.length;
                 
                    this.data.push(json.value[i].Category); 
                     this.new=new Set (this.data);
                    } 
            
                let temp = [];
                this.new.forEach(v => temp.push(v))//convert set to array
            this.setState({category:temp});
            //release memory
            this.new=[];
            this.data=[];
            temp=[]; //test git
         })
        })
       

  }

  componentDidMount(){
    let lists=this.getEventsList();
    this.getCatgeory();
  }

  componentDidUpdate(){
 
  }
 


  public render(): React.ReactElement<ICalendarPanelProps> {
    return (
      <div className={ styles.calendarPanel }>
        <div className={ styles.container }>
         <p> Event List:</p>
          {this.state.eventListName.map((item,index)=>
          <li>{index}{item}</li>
           )}
           <p>EVENT LIST CHOSEN{this.props.eventList}</p>

           <p>Category:</p>
           {this.state.category.map((i)=>
           <li>{i}</li>
           )}


           
         
        </div>
      </div>
    );
  }
}
