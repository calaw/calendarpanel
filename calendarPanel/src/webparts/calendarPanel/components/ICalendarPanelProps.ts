import { PageContext } from "@microsoft/sp-page-context";
import { SPHttpClient, SPHttpClientResponse } from '@microsoft/sp-http';

export interface ICalendarPanelProps {
  description: string;
  pagecontext: PageContext;
  SPHttpClient: SPHttpClient;

}
