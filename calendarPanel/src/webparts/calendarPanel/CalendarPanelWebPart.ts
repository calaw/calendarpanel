import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import * as strings from 'CalendarPanelWebPartStrings';
import CalendarPanel from './components/CalendarPanel';
import { ICalendarPanelProps } from './components/ICalendarPanelProps';

export interface ICalendarPanelWebPartProps {
  description: string;
}

export default class CalendarPanelWebPart extends BaseClientSideWebPart <ICalendarPanelWebPartProps> {

  public render(): void {
    const element: React.ReactElement<ICalendarPanelProps> = React.createElement(
      CalendarPanel,
      {
        description: this.properties.description,
        pagecontext: this.context.pageContext,
				SPHttpClient: this.context.spHttpClient


      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
