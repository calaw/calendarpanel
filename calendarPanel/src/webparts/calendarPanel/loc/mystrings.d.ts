declare interface ICalendarPanelWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'CalendarPanelWebPartStrings' {
  const strings: ICalendarPanelWebPartStrings;
  export = strings;
}
